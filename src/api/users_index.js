
// 用户管理接口模块

// 引入axios
import axios from 'axios'
// 设置接口基准路径
axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么?获取token,传递
  // 1.获取token
  var token = localStorage.getItem('Vueproject_token')
  if (token) {
    // 2.在发送请求的时候传递token
    config.headers['Authorization'] = token
  }
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 登录验证
export const login = (obj) => {
//   console.log(obj)
  // axios.post返回的是一个promise对象
  return axios.post('login', obj).then((result) => {
    //   .then这个方法中的返回值会返回到这个then方法中
    return result.data
  })
}

// 获取用户数据
export const getUserList = (pgdata) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.get('users', { params: pgdata }).then((result) => {
    return result.data
  })
}

// 添加用户
export const addUser = (params) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.post('users', params).then((result) => {
    return result.data
  })
}

// 编辑用户
export const editUser = (params) => {
  console.log(params.id)
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.put('users/' + params.id, params).then((result) => {
    return result.data
  })
}

// 删除用户
export const delUser = (id) => {
  // console.log(id)
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.delete(`users/${id}`).then((result) => {
    return result.data
  })
}

// 修改用户状态
export const statuUser = (params) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.put(`users/${params.id}/state/${params.mg_state}`).then((result) => {
    return result.data
  })
}

// 动态加载左侧菜单
export const getAllMenus = () => {
  // 这种方式默认是get
  return axios({
    url: 'menus'
  })
}
