
// 商品管理模块
import axios from './common.js'

// 获取分类参数数据
export const getAllParams = (type) => {
  return axios.get('categories', [type]).then((result) => {
    return result.data
  })
}

// 获取商品列表数据
export const getAllgoods = (params) => {
  return axios.get('goods', { params }).then((result) => {
    return result.data
  })
}

// 添加商品
export const addGoods = (params) => {
  return axios.post('goods', params).then((result) => {
    return result.data
  })
}

// 删除商品
export const delGoods = (id) => {
  return axios.delete(`goods/${id}`).then(result => result.data)
}

// 编辑商品
export const editGoods = (params) => {
  return axios.put(`goods/${params.goods_id}`, params).then(result => result.data)
}
