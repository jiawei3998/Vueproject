
// 授权管理接口模块

// 引入axios
import axios from 'axios'
// 设置接口基准路径
axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么?获取token,传递
  // 1.获取token
  var token = localStorage.getItem('Vueproject_token')
  if (token) {
    // 2.在发送请求的时候传递token
    config.headers['Authorization'] = token
  }
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 获取角色列表
export const getRolesList = () => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.get(`roles`).then((result) => {
    return result.data
  })
}

// 授权管理
export const roleUser = (params) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.put(`users/${params.id}/role`, params).then((result) => {
    return result.data
  })
}

// 获取权限列表
export const getRootList = (type) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.get(`rights/${type}`).then((result) => {
    return result.data
  })
}

// 添加角色
export const addPart = (params) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.post('roles', params).then((result) => {
    return result.data
  })
}

// 删除角色
export const delRole = (id) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.delete(`roles/${id}`).then((result) => {
    return result.data
  })
}

// 移除指定角色的指定权限
export const removeRoleTag = (roleId, tagId) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.delete(`roles/${roleId}/rights/${tagId}`).then((result) => {
    return result.data
  })
}

// 获取权限列表
export const RootList = () => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.get(`rights/tree`).then((result) => {
    return result.data
  })
}

// 编辑角色
export const editRole = (params) => {
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.put(`roles/${params.id}`, params).then((result) => {
    return result.data
  })
}

// 角色授权
export const addRoot = (roleId, rids) => {
  // console.log(roleId, rids)
  // get方式获取数据需要用params对象,里面传属性=值(对象)
  return axios.post(`roles/${roleId}/rights`, { rids }).then((result) => {
    return result.data
  })
}
