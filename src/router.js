
// 引入vue
import Vue from 'vue'
// 引入路由
import VueRouter from 'vue-router'
import Login from '@/views/Login.vue'
import Home from '@/views/Home.vue'
import Welcom from '@/views/Welcom.vue'
import User from '@/views/users/User.vue'
import Roles from '@/views/role/Roles.vue'
import Rights from '@/views/role/Rights.vue'
import Goods from '@/views/goods/Goods.vue'
import List from '@/views/goods/list.vue'
import Add from '@/views/goods/add.vue'
import Params from '@/views/goods/Params.vue'
import Categories from '@/views/goods/Categories.vue'
import Orders from '@/views/orderData/orders.vue'
import Reports from '@/views/orderData/reports.vue'
// 使用路由
Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    { name: 'default', path: '/', redirect: { name: 'Login' } },
    { name: 'Login', path: '/login', component: Login },
    { name: 'Home',
      path: '/home',
      component: Home,
      redirect: { name: 'Welcom' },
      children: [
        { name: 'Welcom', path: '/welcom', component: Welcom },
        { name: 'User', path: 'users', component: User },
        { name: 'Roles', path: 'roles', component: Roles },
        { name: 'Rights', path: 'rights', component: Rights },
        { name: 'Goods',
          path: 'goods',
          component: Goods,
          redirect: { name: 'List' },
          children: [
            { name: 'List', path: 'list', component: List },
            { name: 'Add', path: 'add', component: Add }
          ] },
        { name: 'Params', path: 'params', component: Params },
        { name: 'Categories', path: 'categories', component: Categories },
        { name: 'Orders', path: 'orders', component: Orders },
        { name: 'Reports', path: 'reports', component: Reports }
      ] }
  ]
})
