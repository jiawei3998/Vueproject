
import Vue from 'vue'
import Vuex from 'vuex'
// 使用Vuex
Vue.use(Vuex)
// 储存数据的对象
const state = {
  currentUerName: ''
}
// 对state操作的方法
const mutations = {
  setUserName (state, value) {
    state.currentUerName = value
  }
}
// 包含触发mutations中函数的方法
const actions = {
  setUserNameAction: ({ commit }, value) => {
    commit('setUserName', value)
  }
}
const getters = {
  getUserName (state) {
    return state.currentUerName
  }
}
// 默认返回Vuex实例
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
