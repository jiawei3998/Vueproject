// 引入vue
import Vue from 'vue'
import App from './App.vue'
// 引入路由模块
import router from '@/router.js'
// 引入Vuex模块
import store from './store.js'

// 引入index样式文件
import './styles/index.less'
// 引入富文本框
import VueQuillEditor from 'vue-quill-editor'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 引入element-ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(VueQuillEditor)
// 使用ElementUI
Vue.use(ElementUI)

// 添加导航守卫
router.beforeEach((to, from, next) => {
  console.log(to)
  // 判断是否有token值
  var token = localStorage.getItem('Vueproject_token')
  if (token || to.path === '/login') {
    next()
  } else {
    next({ name: 'Login' })
  }
})

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
